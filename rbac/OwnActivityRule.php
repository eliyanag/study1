<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
use app\models\User;
use app\models\Activity;

class OwnActivityRule extends Rule
{
	public $name = 'ownActivityRule';
	
	public function execute($user, $item, $params)
	{
		if(isset($_GET['id'])){
				$userCategory = User::findOne($user);
				$activitytCategory = Activity::findOne($_GET['id']);
				//������ �� ��� ����� �� ���� �� ����� �� �������� ��� ������ ������� ���� ����
				if(isset($userCategory) && isset($activitytCategory)){
					if($userCategory->CategoryId == $activitytCategory->categoryId)
						return true;
				}
			}
		return false;
	}
}